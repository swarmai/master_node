#! /usr/bin/env python

import rospy
from points_regions import Point, Points_Regions_Listener
from telem_functions import Telem_Talker
from mavlink_functions import *
import sys
from telem_class import Telem, Waypoint
from system_info import System_Info_Drone
from drone_logic import drone_logic

def drone_main(address, number):
    rospy.init_node('drone_' + str(number))
    sysinfo = System_Info_Drone(int(number), address)
    drone_logic(sysinfo)
    
    # global points_regions_master 
    # points_regions_master = Points_Regions_Listener();
    # global points_regions_self
    # points_regions_self = Points_Regions_Listener('pr_talker_' + str(number))

    # global telem_self
    # telem_self = Telem_Talker(address, 'telem_talker_' + str(number))
    # home = telem_self.callback_telem
    # waypoints=[]
    # waypoints.append(Waypoint(home.lat, home.lon, home.alt)) #first waypoint is home position
    # waypoints.append(Waypoint(-35.365775, 149.163419, 100))
    # inp = 'new'
    # while inp != 'exit':
    #     inp = raw_input()
    #     print(points_regions_master.CompleteMission)
    #     print(points_regions_self.CompleteMission)
    #     telem_self.send_wpts(waypoints)
    #     if points_regions_master.rtl == True or points_regions_self.rtl == True:
    #         telem_self.rtl = True
    #         points_regions_master.rtl = False
    #         points_regions_self.rtl = False
    #     if points_regions_master.liftoff == True or points_regions_self.liftoff == True:
    #         telem_self.liftoff = True
    #         points_regions_self.liftoff = False
    #         points_regions_master.liftoff = False

if __name__ == '__main__':
    if len(sys.argv) == 3:
        number = sys.argv[1]
        address = sys.argv[2]
    elif len(sys.argv) == 2:
        number = sys.argv[1]
        address = 'udpin:0.0.0.0:'+str(14550+10*int(number))
    else:
        sys.exit(1)
    try:
        drone_main(address, number)
    except rospy.ROSInterruptException:
        pass

