#! /usr/bin/env python

import rospy
import sys
from points_regions import Point, Points_Regions_Talker
from telem_functions import Telem_Listener
from telem_class import Telem
from system_info import System_Info_Main
from master_logic import master_logic
import time
import threading


points_regions_master = None
points_regions_individual = []

telem_self = []


            





def master_main(number):
    rospy.init_node('drone_master')
    sysinfo = System_Info_Main(int(number), "/home/springmind/swarm_gui/flask_attempt/app/static/dat/pts_regions.pr", "/home/springmind/swarm_gui/flask_attempt/app/static/dat/telem.tl")


    master_logic(sysinfo)
    # global points_regions_master 
    # points_regions_master = Points_Regions_Talker();
    # global points_regions_individual

    # for i in range(int(number)):
    #     points_regions_individual.append(Points_Regions_Talker('pr_talker_' + str(i)))
    #     telem_self.append(Telem_Listener('telem_talker_' + str(i)))

    # input_file = fileReader("/home/nathan/catkin_ws/src/master_node/pts_regions.pr")
     
    #  while True:
    #     points_regions_input_lock.acquire()
    #     # rospy.loginfo(points_regions_input)
    #     points_regions_master.update_points_regions(points_regions_input)
    #     points_regions_self[0].update_points_regions(points_regions_input[0:2])
    #     points_regions_self[1].update_points_regions(points_regions_input[1:2])
    #     points_regions_input_lock.release()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        number = sys.argv[1]
    else:
        sys.exit(1)
    try:
        master_main(number)
    except rospy.ROSInterruptException:
        pass

