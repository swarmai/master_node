#!/usr/bin/env python

import rospy
from swarm_ai.msg import *
import threading
from mavlink_functions import *
from telem_class import Telem  

class Telem_Listener:
    def __init__(self, subscribe_to='telem_talker'):
        self.Telemetry=None
        self.subscribe_to = subscribe_to
        self.init_telem_listener()

    def callback(self, telemetry):
        self.Telemetry=Telem(telemetry.lat,telemetry.long,telemetry.alt,telemetry.hdg,telemetry.time)

    def init_telem_listener(self):

        # In ROS, nodes are uniquely named. If two nodes with the same
        # name are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        
        #rospy.init_node('pr_listener_' + str(self.drone_number), anonymous=True)

        rospy.Subscriber(self.subscribe_to, telemetry, self.callback)

        #rospy.loginfo("HERE")

        # spin() simply keeps python from exiting until this node is stopped
        # rospy.spin()

class Telem_Talker:
    def telem_callback(self, event):
        self.callback_telem = self.connection.request_position()
        if self.wpts_to_send == True:
            self.connection.send_waypoints(self.wpts)
            self.wpts_to_send = False   
        if self.rtl == True:
            self.connection.set_mode("RTL")
            self.rtl = False
        if self.liftoff == True:
            self.connection.liftoff()
            self.liftoff = False


    def __init__(self, address, pub_name = 'telem_talker'):
        self.address = address
        self.pub_name = pub_name
        self.rate = rospy.Rate(2)
        self.pub = rospy.Publisher(self.pub_name, telemetry, queue_size=10)
        self.connection=Drone(address)
        self.connection.request_telem()
        self.callback_telem=self.connection.request_position()
        self.home = self.callback_telem
        self.telem_timer = rospy.Timer(rospy.Duration(.05), self.telem_callback, oneshot=False)
        self.thread = threading.Thread(target=self.thread_func)
        self.thread.setDaemon(True)
        self.thread.start()
        self.wpts_to_send = False
        self.liftoff = False
        self.rtl = False
        self.wpts = []
        

    def send_wpts(self, wpts):
        wpts.insert(0, self.home)
        self.wpts_to_send = True
        self.wpts = wpts

    def thread_func(self):
        while not rospy.is_shutdown():
            loc_telem = self.callback_telem
            # rospy.loginfo(str(loc_telem.alt))
            self.pub.publish(telemetry(loc_telem.hdg, loc_telem.lat, loc_telem.lon, loc_telem.alt, loc_telem.time))
            self.rate.sleep()            
