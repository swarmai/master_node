
#! /usr/bin/env python
import math


R = 6371000  # Radius of the earth
#PI = 3.141592653589

# cartesian coordinates of some point

class cartCoords:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"
    def __repr__(self):
        return self.__str__()  

# longitude and latitude give a point on earth

class gpsLoc:
    def __init__(self, longitude, latitude, isCorner = False):
        self.longitude = longitude
        self.latitude = latitude
        self.isCorner = isCorner
    def __str__(self):
        return "(" + str(self.latitude) + ", " + str(self.longitude) + ")"
    def __repr__(self):
        return self.__str__()  

class waypoints:
    def __init__(self, points):
        self.points = points
    def __str__(self):
        return str(len(self.points)) + " waypoints: " + str(self.points)

#   c1 - 1st corner of rectangle in gps coordinates
#   c2 - 2nd corner of rectangle in gps coordinates
#   deg     - degrees from North (counter-clockwise)*

#   *note: one set of edges is parallel to this angle and the other is
#          perpendicular

class rectAreaGPS:
    def __init__(self, c1, c2, deg):
        self.c1 = c1
        self.c2 = c2
        self.deg = deg
    def __str__(self):
        return str(self.c1) + str(self.c2)
    def __repr__(self):
        return self.__str__()  
 
#def allocRectAreaGPS(x):

#def allocRectC(x):

#def freeRectAreaGPS(x):


#   arguments:
#      point - point to be rotated
#      ang   - how much to rotate (counter-clockwise)

#   return value:
#     cartCoord: point rotated through angle

# cartCoords *rotate(cartCoords* point, double ang);


#    arguments:
#       area - a rectangular area structure which is the are to be partitioned
#       n    - number of partitions

#    return value:
#       rectAreaGPS*: array of rectAreaGPS structures each representing a
#       discrete partition of the area

# rectAreaGPS *partition(rectAreaGPS* area, int n);


#   arguments:
#     0: name of file
#     1: latitude1
#     2: longitude1
#     3: latitude2
#     4: longitude2
#     5: degrees from north counter clockwise
#     6: number of partitions


# rectAreaGPS* partition_main (double c1Lat, double c1Lng, double c2Lat, double c2Lng,
#   double deg, int numPars);

# waypoints waypoints_main (double c1Lat, double c1Lng, double c2Lat, double c2Lng,
#     double deg, double alt);

# void allocRectAreaGPS(rectAreaGPS *x) {
#   x = (rectAreaGPS*) malloc(sizeof(rectAreaGPS));
#   x.c1 = (gpsLoc*) malloc(sizeof(gpsLoc));
#   x.c2 = (gpsLoc*) malloc(sizeof(gpsLoc));
# }

# void allocRectC(rectAreaGPS *x) {
#   x.c1 = (gpsLoc*) malloc(sizeof(gpsLoc));
#   x.c2 = (gpsLoc*) malloc(sizeof(gpsLoc));
# }

# void freeRectAreaGPS(rectAreaGPS *x) {
#   free(x.c1);
#   free(x.c2);
#   free(x);
# }

# /*
#   arguments:
#      point - point to be rotated
#      ang   - how much to rotate (counter-clockwise)

#   return value:
#     cartCoord: point rotated through angle
# */

def rotate(point, ang):
    x_ = point.x*math.cos(ang) - point.y*math.sin(ang)
    y_ = point.x*math.sin(ang) + point.y*math.cos(ang)
    return cartCoords(x_, y_)


# /*
#    arguments:
#       area - a rectangular area structure which is the are to be partitioned
#       n    - number of partitions

#    return value:
#       rectAreaGPS*: array of rectAreaGPS structures each representing a
#       discrete partition of the area
# */

def partition(area, n):
    global R
    #   cartCoords *normC2, *temp1, *temp2;
    #   double X, Y, dX, x0, x1, y0, y1, baseLat;
    #   rectAreaGPS *toReturn = (rectAreaGPS*) malloc(sizeof(rectAreaGPS) * n);

    #toReturn = (rectAreaGPS*)malloc(sizeof(rectAreaGPS) * n);
    toReturn = []
    #   normC2 = (cartCoords*) malloc(sizeof(cartCoords));
    #   temp1 = (cartCoords*) malloc(sizeof(cartCoords));
    #   temp2 = (cartCoords*) malloc(sizeof(cartCoords));
    baseLat = area.c1.latitude/180 * math.pi

    # Translate points so that corner 1 is located at (0,0)
    #std::cout <<area.c2.longitude << " " << area.c1.longitude << std::endl;
    normC2 = cartCoords((area.c2.longitude - area.c1.longitude)/180 * math.pi * R * math.cos(baseLat),R * (area.c2.latitude - area.c1.latitude)/180 * math.pi)

    #std::cout << "xNormC2: " << normC2.x << " yNormC2 " << normC2.y << std::endl;

    # rotate second corner in clockwise direction so that we only have to worry
    # about x and y coordinates
    normC2 = rotate(normC2, (-1) * area.deg)

    X = normC2.x
    Y = normC2.y
    dX = X/n  # width of partition

    for i in range(n):

        #allocRectC(&toReturn[i]);
        # temp1 and temp2 represent the unrotated, untranslated points
        temp1 = cartCoords((dX * i), 0)
        temp2 = cartCoords(dX * (i + 1),Y)

        # rotate the temp points using the degrees from north
        temp1 = rotate(temp1, area.deg)
        temp2 = rotate(temp2, area.deg)

        # translate points and store them
        c1lon = 180 * temp1.x/(R * math.cos(baseLat) * math.pi) + area.c1.longitude
        c1lat = 180 * temp1.y/(R * math.pi) + area.c1.latitude
        c2lon = 180 * temp2.x/(R * math.cos(baseLat) * math.pi) + area.c1.longitude
        c2lat = 180 * temp2.y/(R * math.pi) + area.c1.latitude
        toReturn.append(rectAreaGPS(gpsLoc(c1lon, c1lat), gpsLoc(c2lon, c2lat), area.deg))

        # std::cout << n << "c1x: " << toReturn[i].c1.longitude << " c1y: " <<
        #   toReturn[i].c1.latitude << " c2x: " << toReturn[i].c2.longitude <<
        #   " c2y: " <<   toReturn[i].c2.latitude << std::endl;

    return toReturn


def partition_main(c1Lat, c1Lng, c2Lat, c2Lng, deg, numPars):
    c1 = gpsLoc(c1Lng, c1Lat)
    c2 = gpsLoc(c2Lng, c2Lat)
    area = rectAreaGPS(c1, c2, deg / 180 * math.pi)
    return partition(area, numPars)


def waypoints_main(c1Lat, c1Lng, c2Lat, c2Lng, deg, alt):
    global R
    #  std::cout << std::fixed;
    #  std::cout << std::setprecision(9);

    xfov = 1.237637553 * alt
    yfov = 0.936868459 * alt

    #std::cout << "xfov: " << xfov << " yfov: " << yfov << std::endl;

    #/*double xDelt = R * (c2Lng - c1Lng) * cos(deg);
    #double yDelt = R * (c2Lat - c1Lat);

    #std::cout << "xdelt: " << xDelt << " yDelt: " << yDelt << " rayangle : " << rayAngle << std::endl;

    #*/

    #cartCoords *normC2, *temp1, *temp2;
    #double X, Y, dX, x0, x1, y0, y1, baseLat;

    #normC2 = (cartCoords*) malloc(sizeof(cartCoords));
    baseLat = c1Lat/180 * math.pi

    # Translate points so that corner 1 is located at (0,0)
    #std::cout << c2Lng << " " << c1Lng << std::endl;
    normC2 = cartCoords((c2Lng - c1Lng)/180 * math.pi * R * math.cos(baseLat), R * (c2Lat - c1Lat)/180 * math.pi)
    #std::cout << "xNormC2: " << normC2.x << " yNormC2 " << normC2.y << std::endl;

    # rotate second corner in clockwise direction so that we only have to worry
    # about x and y coordinates
    normC2 = rotate(normC2, (-1) * deg)

    l1 = c1Lng * math.pi / 180.0
    l2 = c2Lng * math.pi / 180.0

    xDist = abs(normC2.x)
    yDist = abs(normC2.y)

    #get the number of x postitions. Overrun later
    xPos = xDist /(xfov)
    yPos = yDist /(yfov) 

    #waypoints returner
    #the xPos and yPos was sometimes negative
    #returner.size = abs((xPos+1) * (yPos + 1))
    #returner.points = [gpsLoc(0, 0)] * returner.size
    pts = []
    for k in range(abs(int(xPos + 1))*abs(int(1 + 1))):
        pts.append(gpsLoc(0,0))
    returner = waypoints(pts)
    yMult = 1

    xStarter = c2Lng
    if c1Lng  < c2Lng:
        xStarter = c1Lng

    yStarter = c2Lat
    if c1Lat < c2Lat:
        yStarter = c1Lat

    yPos = 1

    # std::cout << "xStart: " << xStarter << " ySTart: " << yStarter << std::endl;
    # print("xpos: " + str(abs(int(xPos + 1))) + " ypos: " + str(abs(int(yPos + 1))));
    for i in range(abs(int(xPos + 1))):
        for j in range(abs(int(yPos + 1))):
            #come up with a portion of the x-distance
            # print(str(xStarter) +  " BS " + str(yStarter) + " " + str(yMult))
            x = i  / xPos * xDist
            #want a snake pattern.
            if yMult == 1:
                #regular direction
                y = j / yPos * yDist 
            else:
                #go from top to bottom
                y = (int(yPos) - j)  / yPos * yDist

            normC2.x = x
            normC2.y = y

            #std::cout << x << " BS " << y << " " << yMult << std::endl;

            normC2 = rotate(normC2, deg)

            #std::cout << normC2.x << " BS2 " << normC2.y << "\n" << std::endl;

            x = normC2.x / (R * math.cos(baseLat)) * 180.0 / math.pi + xStarter
            y = normC2.y / R  * 180.0 / math.pi + yStarter

            returner.points[i * (int(yPos) + 1) + j].latitude = y
            returner.points[i * (int(yPos) + 1) + j].longitude = x

            if j == int(yPos):
                yMult *= -1

            if j == 0 or j == yPos:
                returner.points[i * (int(yPos) + 1) + j].isCorner = True

    return returner
