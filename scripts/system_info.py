from telem_functions import Telem_Listener, Telem_Talker
from points_regions import Point, Points_Regions_Talker, Points_Regions_Listener
import threading, time
import rospy
import os


class fileReader:
    def __init__(self, path, sender=None):
        self.path = path
        self.points_regions_input = []
        self.points_regions_input_lock = threading.Lock()
        self.sender = None
        self.file_thread = threading.Thread(target=self.read_pr_file)
        self.file_thread.setDaemon(True)
        self.file_thread.start()

    def getPoints(self):
        self.points_regions_input_lock.acquire()
        returner =  self.points_regions_input
        self.points_regions_input_lock.release()
        return returner

    def read_pr_file(self):
        while True:
            f = open(self.path, "r")
            points_regions_input_new = []
            for ln in f:
                # rospy.loginfo(ln)
                if ln == '[Index]#[Group]#[Region]#[Region_index]#[Lat]#[Long]#[Alt]\n':
                    continue
                ln_array = ln.split('#')
                if(len(ln_array) != 7):
                    rospy.loginfo("File has bad format")
                    return
                lat = float(ln_array[4].strip())
                lon = float(ln_array[5].strip())
                alt = float(ln_array[6].strip())
                idx = int(ln_array[0].strip())
                group = int(ln_array[1].strip())
                reg = int(ln_array[2].strip())
                reg_idx = int(ln_array[3].strip())
                points_regions_input_new.append(Point(lat, lon, alt, idx, group, reg, reg_idx))
            f.close()    
            self.points_regions_input_lock.acquire()
            self.points_regions_input = points_regions_input_new
            self.points_regions_input_lock.release()
            if self.sender is not None:
                self.sender.update_points_regions(self.points_regions_input)
            time.sleep(5)


class System_Info(object):
    def __init__(self, number):
        self.number = number
        self.pr_main = None
        self.telem_subscribers = {}
        self.publish_telem = None

    def sub_telem(self, nums):
        for num in nums:
            self.telem_subscribers[num] = Telem_Listener('telem_talker_' + str(num))



class System_Info_Drone(System_Info):
    def __init__(self, number, address):
        super(System_Info_Drone, self).__init__(number)
        self.address = address
        self.pr_main = Points_Regions_Listener();
        self.pr_self = Points_Regions_Listener('pr_talker_' + str(self.number))
        self.telem = Telem_Talker(self.address, 'telem_talker_' + str(self.number))


class System_Info_Main(System_Info):
    def __init__(self, number, pts_address, telem_address):
        super(System_Info_Main, self).__init__(number)
        self.sub_telem([ i for i in range(self.number)])
        self.pr_talkers = []
        self.pr_main = Points_Regions_Talker();
        self.start_points_regions([ i for i in range(self.number)])
        self.input_file = fileReader(pts_address)
        self.telem_address = telem_address
        self.pipe_thread = threading.Thread(target=self.write_telem)
        self.pipe_thread.setDaemon(True)
        self.pipe_thread.start()

        
    def write_telem(self):
        while True:
            self.pipe = open(self.telem_address, "w")
            returner = ""
            for drone_no in self.telem_subscribers.keys():
                curr_telem = self.telem_subscribers[drone_no].Telemetry
                # print(curr_telem)
                if curr_telem is None:
                    continue;
                returner += str(drone_no) + "#" + str(curr_telem.lat) + "#" + str(curr_telem.lon) + "#" + str(curr_telem.alt) + "\n"
            self.pipe.write(returner)    
            self.pipe.close()
            time.sleep(.5)



    def start_points_regions(self, nums):
        for num in nums:
            self.pr_talkers.append(Points_Regions_Talker('pr_talker_' + str(num)))
            