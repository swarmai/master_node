from points_regions import Point, Points_Regions_Talker
from telem_functions import Telem_Listener
from telem_class import Telem
from system_info import System_Info_Main
from partition_utils import waypoints_main
from telem_class import Waypoint


def drone_logic(sysinfo):
    # If you want to enable the start button in the GUI
    sysinfo.pr_main.set_takeoff_trigger(sysinfo.telem)
    alt = 30 + sysinfo.number

    curr_pts = []
    while True:
        # print("HI")
        new_pts = sysinfo.pr_self.CompleteMission
        if(curr_pts == new_pts):
            continue

        curr_pts = new_pts
        print(curr_pts)

        if(len(curr_pts) < 3):
            continue

        pts = waypoints_main(curr_pts[1].lat, curr_pts[1].lon, curr_pts[2].lat, curr_pts[2].lon, 0, alt)
        #print(pts)
            
        snd_pts = []    
        for pt in pts.points:
            snd_pts.append(Waypoint(pt.latitude, pt.longitude, alt))

        sysinfo.telem.send_wpts(snd_pts)    



