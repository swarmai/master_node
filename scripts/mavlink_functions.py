#! /usr/bin/env python

from pymavlink import mavutil, mavwp
import time
from telem_class import Telem, Waypoint
#import rospy
 

class Drone:
    def __init__(self,address = 'udpin:0.0.0.0:14550'):
        #for testing, udp port number is 14550 + 10*(ardupilot id) 
        # i.e. sim_vehicle.py -I 1 is port 14560 
        self.address=address
        self.connection = mavutil.mavlink_connection(address)
        self.connection.wait_heartbeat()

    def arm(self):
        self.connection.mav.command_long_send(
        1, # autopilot system id
        1, # autopilot component id
        400, # command id, ARM/DISARM
        0, # confirmation
        1, # arm!
        0,0,0,0,0,0 # unused parameters for this command
        )

    def disarm(self):
        self.connection.mav.command_long_send(
        1, # autopilot system id
        1, # autopilot component id
        400, # command id, ARM/DISARM
        0, # confirmation
        0, # disarm!
        0,0,0,0,0,0) # unused parameters for this command

    def takeoff(self, alt):
        self.connection.mav.command_long_send(
        1, # autopilot system id
        0, # autopilot component id
        22, # command id, MAV_CMD_NAV_TAKEOFF
        0, # confirmation
        0, # unused 
        0, # 
        0,0,0,0,alt # alt in metres
        )

    def request_telem(self, rate=10):
        self.connection.mav.command_long_send(
        1, # autopilot system id
        0, # autopilot component id
        511, # command id, SET_MESSAGE_INTERVAL
        0, # confirmation
        33, # gps messages
        rate, # rate in Hz (actually dont know what this does)
        0,0,0,0,0 # unused parameters for this command
        )

    def request_position(self):
        while True:
            msg = self.connection.recv_match()
            if not msg:
                #print("not a match")
                continue
            if msg.get_type() == 'GLOBAL_POSITION_INT':
                return Telem(msg.lat/1e7, msg.lon/1e7, msg.alt/1e3, msg.hdg/1e2, msg.time_boot_ms)
                break

    def set_mode(self, mode):

        # Check if mode is available
        if mode not in self.connection.mode_mapping():
            #print('Unknown mode : {}'.format(mode))
            #print('Try:', list(self.mode_mapping().keys()))
            exit(1)

        # Get mode ID
        mode_id = self.connection.mode_mapping()[mode]
        # Set new mode
        # self.connection.mav.command_long_send(
        #    self.connection.target_system, self.connection.target_component,
        #    mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0,
        #    0, mode_id, 0, 0, 0, 0, 0) or:
        # self.set_mode(mode_id) or:
        self.connection.mav.set_mode_send(
            self.connection.target_system,
            mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
            mode_id)

        # Check ACK
        ack = False
        while not ack:
            # Wait for ACK command
            ack_msg = self.connection.recv_match(type='COMMAND_ACK', blocking=True)
            ack_msg = ack_msg.to_dict()

            # Check if command in the same in `set_mode`
            if ack_msg['command'] != mavutil.mavlink.MAVLINK_MSG_ID_SET_MODE:
                continue

            # Print the ACK result !
            #print(mavutil.mavlink.enums['MAV_RESULT'][ack_msg['result']].description)
            break
    def send_waypoints(self,waypoints):
        wp = mavwp.MAVWPLoader()                                                    
        seq = 1
        frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
        radius = 5
        N=len(waypoints)# first waypoint is home
        wp
        for i in range(N):                  
            wp.add(mavutil.mavlink.MAVLink_mission_item_message(self.connection.target_system,
                self.connection.target_component,
                seq,
                frame,
                mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                0, 0, 0, radius, 0, 0,
                waypoints[i].lat, # latitude - if these are not in the valid range, ardupilot crashes
                waypoints[i].lon, # longitude
                waypoints[i].alt)) #altitude in metres
            seq += 1
            if i == 0:
                wp.add(mavutil.mavlink.MAVLink_mission_item_message(self.connection.target_system,
                    self.connection.target_component,
                    seq,
                    frame,
                    mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                    0, 0, 0, radius, 0, 0,
                    waypoints[i].lat, # latitude - if these are not in the valid range, ardupilot crashes
                    waypoints[i].lon, # longitude
                    30)) #altitude in metres
                seq += 1                                                                       
        wp.add(mavutil.mavlink.MAVLink_mission_item_message(self.connection.target_system,
            self.connection.target_component,
            seq,
            frame,
            mavutil.mavlink.MAV_CMD_NAV_RETURN_TO_LAUNCH,
            0, 0, 0, radius, 0, 0,
            waypoints[0].lat, # latitude - if these are not in the valid range, ardupilot crashes
            waypoints[0].lon, # longitude
            waypoints[0].alt)) #altitude in metres
        self.connection.waypoint_clear_all_send()                                     
        self.connection.waypoint_count_send(wp.count())                          

        for i in range(wp.count()):
            msg = self.connection.recv_match(type=['MISSION_REQUEST'],blocking=True)             
            self.connection.mav.send(wp.wp(msg.seq))    

    def liftoff(self):
        self.set_mode("GUIDED")
        self.arm()
        self.takeoff(30)
        time.sleep(1)
        self.set_mode("AUTO")
        

#for testing
# if __name__ == '__main__':
    
#     test_drone = Drone()
#     test_drone.request_telem()
#     #while True:
#     for i in range(100):
#         pos=test_drone.request_position()
#         print(str(pos.time))
    
#     #test_drone.liftoff()
#     home=Waypoint(pos.lat,pos.lon,pos.alt)
#     pt1=Waypoint(-35.36, 149.16, 100)
#     pt2=Waypoint(-35.365775, 149.163419, 100)
#     waypoints=[home,pt1,pt2]
#     test_drone.send_waypoints(waypoints)

#     for i in range(100):
#         pos=test_drone.request_position()
#         print(str(pos.time))
    #test_drone.set_mode("AUTO")

    #test_drone.set_mode("RTL")