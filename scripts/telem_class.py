#! /usr/bin/env python

class Telem:
    def __init__(self,lat,lon,alt,hdg,time):
        self.lat=lat
        self.lon=lon
        self.alt=alt
        self.hdg=hdg
        self.time=time

    def __str__(self):
        return ("| Lat = "+str(self.lat)+
        ", Lon = "+str(self.lon)+
        ", Alt = "+str(self.alt) + 
        ", Hdg = "+str(self.hdg) + 
        ", Time = "+str(self.time) + "|")

    def __repr__(self):
        return self.__str__()   

class Waypoint:
    def __init__(self,lat,lon,alt):
        self.lat=lat
        self.lon=lon
        self.alt=alt

    def __str__(self):
        return ("| Lat = "+str(self.lat)+
        ", Lon = "+str(self.lon)+
        ", Alt = "+str(self.alt) + "|")

    def __repr__(self):
        return self.__str__()   