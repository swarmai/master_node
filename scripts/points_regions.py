#!/usr/bin/env python

import rospy
from swarm_ai.msg import *
import threading
#from points_regions.treedict import *

class Point:
    def __init__(self,lat,lon,alt,index,group,region,region_index):
        self.region_index=region_index
        self.region=region
        self.group=group
        self.lat=lat
        self.lon=lon
        self.alt=alt
        self.index=index

    def __str__(self):
        return ("| Region Index = "+str(self.region_index)+
        ", Region = "+str(self.region)+
        ", Group = "+str(self.group)+
        ", Lat = "+str(self.lat)+
        ", Lon = "+str(self.lon)+
        ", Alt = "+str(self.alt)+
        ", Index = "+str(self.index) + "|")

    def __repr__(self):
        return self.__str__()    

    def __eq__(self, other):
        return(
            self.region_index == other.region_index and
            self.region == other.region and
            self.group == other.group and
            self.lat == other.lat and
            self.lon == other.lon and
            self.alt == other.alt and
            self.index == other.index
        )


class Points_Regions_Listener:
    def __init__(self, subscribe_to='pr_talker_main'):
        self.SendableMission = []
        self.WorkingMission = []
        self.CompleteMission = []
        self.liftoff = False
        self.rtl = False
        self.last_index = -1
        self.subscribe_to = subscribe_to
        self.telem_talker = None
        self.init_pr_listener()

    def callback(self, pointstream):
        self.WorkingMission.append(Point(pointstream.lat,pointstream.long,pointstream.alt,pointstream.index,
            pointstream.group, pointstream.region, pointstream.region_index))

        if self.liftoff == False and pointstream.index == -1:
            if self.telem_talker is not None:
                self.telem_talker.liftoff = True
            self.rtl = False
            self.liftoff = True

        if self.rtl == False and pointstream.index == 0:
            if self.telem_talker is not None:
                self.telem_talker.rtl = True
            self.rtl = True
            self.liftoff = False

        if pointstream.index == pointstream.end_index:
            if len(self.WorkingMission) > 0 and self.WorkingMission[0].index == 0:
                self.CompleteMission = self.WorkingMission
            self.WorkingMission = []
            

    def set_takeoff_trigger(self, telem_talker):
        self.telem_talker = telem_talker

    def init_pr_listener(self):

        # In ROS, nodes are uniquely named. If two nodes with the same
        # name are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        
        #rospy.init_node('pr_listener_' + str(self.drone_number), anonymous=True)

        rospy.Subscriber(self.subscribe_to, pointstream, self.callback)

        #rospy.loginfo("HERE")

        # spin() simply keeps python from exiting until this node is stopped
        # rospy.spin()

class Points_Regions_Talker:
    def __init__(self, pub_name = 'pr_talker_main'):
        self.points=[]
        self.pub_name = pub_name
        self.rate = rospy.Rate(2)
        self.points_lock = threading.Lock()
        self.pub = rospy.Publisher(self.pub_name, pointstream, queue_size=10)
        self.thread = threading.Thread(target=self.thread_func)
        self.thread.setDaemon(True)
        self.thread.start()

    def update_points_regions(self, new_pts):
        self.points_lock.acquire()
        self.points = new_pts
        self.points_lock.release()
            
    def thread_func(self):
        while not rospy.is_shutdown():
            self.points_lock.acquire()
            loc_points = self.points
            self.points_lock.release()
            for point in loc_points:
                # rospy.loginfo(point)
                self.pub.publish(pointstream(point.index, point.group, point.region, point.region_index, point.lat, point.lon, point.alt, loc_points[-1].index))
                self.rate.sleep()
