from points_regions import Point
from telem_functions import Telem_Listener
from telem_class import Telem
from system_info import System_Info_Main
from partition_utils import partition_main



def master_logic(sysinfo):
    # If you want the main topic to automatically publish waypoints from the GUI
    sysinfo.input_file.sender = sysinfo.pr_main

    curr_pts = []
    while True:
        new_pts = sysinfo.input_file.getPoints()
        if(curr_pts == new_pts or  len(new_pts) < 3):
            continue


        curr_pts = new_pts
        # print(curr_pts)

        t_l = curr_pts[1]
        b_r = curr_pts[2]    
        for i in range(1, len(curr_pts)):
            if curr_pts[i].lat > t_l.lat:
                t_l.lat = curr_pts[i].lat
            if curr_pts[i].lon < t_l.lon:
                t_l.lon = curr_pts[i].lon
            if curr_pts[i].lat < b_r.lat:
                b_r.lat = curr_pts[i].lat
            if curr_pts[i].lon > b_r.lon:
                b_r.lon = curr_pts[i].lon      

        parts = partition_main(t_l.lat, t_l.lon, b_r.lat, b_r.lon, 0, sysinfo.number)
        # print(parts)

        for i in range(sysinfo.number):
            snd_pts = [curr_pts[0]] # keeps instruction point
            snd_pts.append(Point(parts[i].c1.latitude, parts[i].c1.longitude, 0, 1, 1, 1,1))
            snd_pts.append(Point(parts[i].c2.latitude, parts[i].c2.longitude, 0, 2, 1, 1,2))
            print(snd_pts)
            sysinfo.pr_talkers[i].update_points_regions(snd_pts)





